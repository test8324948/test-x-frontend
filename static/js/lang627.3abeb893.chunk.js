"use strict";(self.webpackChunk_pcash_app_corporate_bni_new_york=self.webpackChunk_pcash_app_corporate_bni_new_york||[]).push([[12609,28922],{64990:function(e){e.exports=JSON.parse('{"detail":{"title":"Profile","subTitle":"Detail","sections":{"accountInformation":"Account Information","profilePicture":"Profile Picture","changePassword":"Change Password"},"fields":{"userId":{"label":"User ID","placeholder":"Upload Photo"},"userName":{"label":"User Name"},"role":{"label":"Role"},"gender":{"label":"Gender"},"userGroup":{"label":"User Group"},"approvalLevel":{"label":"Approval Level"},"emailAddress":{"label":"Email Address"},"mobilePhoneNo":{"label":"Mobile Phone Number"},"oldPassword":{"label":"Old Password","placeholder":"Enter old Password"},"newPassword":{"label":"New Password","placeholder":"Enter new Password"},"confirmNewPassword":{"label":"Confirm Password","placeholder":"Enter confirm Password"}},"buttons":{"update":"Update","save":"Save"},"noPhoto":"No Photo","modal":{"changePassword":{"title":"Password Updated","message":"Your password has been changed successfully."},"changeProfilePhoto":{"title":"Photo Updated","message":"Your Profile Picture has been changed successfully."}},"errors":{"passwordNotMatch":"New password and confirm password does not match"}}}')},85974:function(e){e.exports=JSON.parse('{"user":{"title":"User","subTitles":{"search":"Cari","add":"Tambah","edit":"Ubah","delete":"Hapus","detail":"Detil"},"modals":{"lockUnlock":{"fail":{"title":"Gagal","message":"Gagal {{action}} user {{user}}. Tolong coba lagi"},"success":{"title":"{{action}} berhasil","message":"Pengguna {{user}} berhasil {{action}}","locked":"Terkunci","unlocked":"tidak dikunci"}},"resetPassword":{"fail":{"title":"Gagal","message":"Gagal mengatur ulang kata sandi untuk {{user}}. Tolong coba lagi"},"success":{"title":"Reset berhasil","message":"Pengguna {{user}} Kata sandi berhasil diatur ulang"}},"forceLogout":{"fail":{"title":"Force logout gagal","message":"Gagal memaksa logout {{users}}. Tolong coba lagi"},"success":{"title":"Force Logout berhasil","message":"Berhasil memaksa pengguna logout"}}},"steps":{"input":"Masukkan","review":"Tinjauan","success":"Sukses"},"filters":{"user_id":{"label":"ID Petugas"},"user_name":{"label":"Nama Petugas"},"user_group":{"label":"Kelompok Petugas"},"user_role":{"label":"Peran Petugas","searchPlaceholder":"Cari Peran Petugas"},"approval_level":{"label":"Tingkat Persetujuan","searchPlaceholder":"Tingkat Persetujuan Pencarian"},"account_group":{"label":"Kelompok Rekening"},"status":{"label":"Status"},"still_login_flag":{"label":"Masih login","true":"Masuk","false":"Belum masuk"}},"fields":{"photo":{"label":"Foto","empty":"Tidak Ada Foto","selectImage":"Pilih gambar","alt":"Petugas"},"userId":{"label":"ID Petugas"},"userName":{"label":"Nama Petugas"},"gender":{"label":"Jenis Kelamin","F":"Perempuan","M":"Pria"},"email":{"label":"Alamat Email"},"mobilePhone":{"label":"Nomor Telepon Seluler"},"userGroup":{"label":"Kelompok Petugas"},"role":{"label":"Peran"},"grantView":{"label":"Beri Akses Lihat Detil Penggajian","false":"Tidak","true":"Ya"},"approvalLevel":{"label":"Tingkat Persetujuan"},"authenticationType":{"label":"Jenis Autentikasi","soft_token":"Soft Token","hard_token":"Hard Token"},"authenticationDevice":{"label":"Perangkat Otentikasi"},"accountGroupName":{"label":"Kelompok Rekening"},"roleName":{"label":"Peran Petugas"},"status":{"label":"Status","LOCKED":"Kunci","ACTIVE":"Penguncian Dibuka"},"deviceNo":{"label":"Token"},"password":{"label":"Kata Sandi","reset":"Atur Ulang"},"action":{"label":"Tindakan","lock":"Kunci","unlock":"Penguncian Dibuka"},"userGroupName":{"label":"Kelompok Petugas"},"deleteStatus":{"label":"Hapus status"}},"buttons":{"back":"Kembali","edit":"Ubah","add":"Tambah","delete":"Hapus","confirm":"Konfirmasi","submit":"Kirim","download":"Unduh","done":"Selesai","forceLogout":"Memaksa untuk Keluar"}}}')}}]);